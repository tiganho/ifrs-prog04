package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import sample.Main;
import sample.model.Person;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    Main main;

    @FXML
    ListView<Person> listview;

    public void onLogout() {
        main.handleLogout();
    }

    public void setMain(Main main) {
        this.main = main;
        listview.setItems(main.getPersonData());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listview.setCellFactory(cell -> new PersonListCellController());
    }
}
