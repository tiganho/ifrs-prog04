package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sample.controller.LoginController;
import sample.controller.MainController;
import sample.controller.PersonOverviewController;
import sample.model.Person;

import java.io.IOException;

// TODO (13) - Crie uma view chamada 'PersonEditDialog'
// TODO (14) - Projete a view para editar e criar novos Person.

//----------------------------------------
//- Label(First Name) ------ TextField   -
//- Label(Last Name)  ------ TextField   -
//-                                      -
//-        Button(Ok) -- Button(Cancel)  -
//----------------------------------------

public class Main extends Application {

    private Stage primaryStage;
    private BorderPane mainView;

    public ObservableList<Person> getPersonData() {
        return personData;
    }

    public void setPersonData(ObservableList<Person> personData) {
        this.personData = personData;
    }

    private ObservableList<Person> personData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        // initializes primary layout
        initMainView();

        setScene(loadLoginView());
    }

    private void setScene(Parent root) {
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/MainView.fxml"));

        mainView = loader.load();

        ((MainController) loader.getController()).setMain(this);
    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/LoginView.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }

    private Parent loadPersonOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/PersonOverviewView.fxml"));

        Parent root = loader.load();

        ((PersonOverviewController) loader.getController()).setMain(this);

        return root;
    }

    public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            // load data from source
            personData.add(new Person("Angelo", "Fabris"));
            personData.add(new Person("Arlei", "Duarte"));
            personData.add(new Person("Christian", "Theobald"));
            personData.add(new Person("Gabriel", "Siqueira"));
            personData.add(new Person("Igor", "Kalb"));
            personData.add(new Person("Jean", "Pereira"));
            personData.add(new Person("Pedro", "Tessaro"));
            personData.add(new Person("William", "Grosseli"));

            try {
                //primaryStage.setScene(loadPersonOverviewView());
                mainView.setCenter(loadPersonOverviewView());
                setScene(mainView);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public void handleLogout() {
        // clear data
        personData.clear();

        try {
            setScene(loadLoginView());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
