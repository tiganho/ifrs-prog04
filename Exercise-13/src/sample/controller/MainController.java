package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import sample.Main;
import sample.model.Person;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    Main main;

    public void onLogout() {
        main.handleLogout();
    }

    public void setMain(Main main) {
        this.main = main;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
