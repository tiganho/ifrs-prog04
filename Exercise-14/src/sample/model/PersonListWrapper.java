package sample.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

// TODO(01 - Anote a classe com '@XmlRootElement(name = "persons")')
public class PersonListWrapper {
    private List<Person> persons;

    // TODO(02) - Anote o método com '@XmlElement(name = "person")'
    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }
}
